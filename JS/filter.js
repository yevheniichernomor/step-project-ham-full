const list = document.querySelector('.list');
const items = document.querySelectorAll('.blocks-item');
const listItems = document.querySelectorAll('.list-item')

function filter(){
    list.addEventListener('click', event => {
        const targetId = event.target.dataset.id;
        const target = event.target; 
        if(target.classList.contains('list-item')){
            listItems.forEach(listItem => listItem.classList.remove('list-item-active'));
            target.classList.add('list-item-active'); 
        } 

        switch(targetId){
            case 'all':
                getItems('blocks-item')
                break
                case 'graphic-design':
                    getItems(targetId)
                    break
                case 'web-design':
                    getItems(targetId)
                    break
                case 'wordpress':
                    getItems(targetId)
                    break
                case 'landing-pages':
                    getItems(targetId)
                    break
            }
    })
}

filter();

function getItems(className) {
    items.forEach(item => {
        if (item.classList.contains(className) && !item.classList.contains('inactive')) {
            item.style.display = 'inline-block'
        } 
        else if(!item.classList.contains('inactive')){
            item.style.display = 'none'
        }        
    })    
}