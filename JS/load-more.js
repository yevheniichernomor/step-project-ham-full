const loader = document.querySelector('.loader');
const loadBtn = document.getElementById('load-more-btn');
const sectionThree = document.querySelector('.section-three');
const blocksBox = document.querySelector('.blocks-box');
const blocksBox2 = document.querySelector('.blocks-box2')

let currentItems = 12;
let currentItems2 = 12;

function loadMore() {        
   
    const elementList = [...document.querySelectorAll('.blocks-box .blocks-item')];
    
    for (let i = currentItems; i < currentItems + 12; i++) {
        if(elementList[i]) {
            elementList[i].classList.remove('inactive');
        }
    }
    currentItems += 12;

    const elementList2 = [...document.querySelectorAll('.blocks-box2 .blocks-item')];
    
    for (let j = currentItems2; j < currentItems2 + 12; j++) {
        if(elementList2[j]) {
            elementList2[j].classList.remove('inactive');
        }
    }    
    currentItems2 += 12;

    if(currentItems >= elementList.length){
        loadBtn.style.display = 'none'
    }
}

let timeID;
function delayedUpload() {
    timeID = setTimeout(loadMore, 2000);
    loader.style.opacity = 100;
    setTimeout(() => {
        loader.style.opacity = 0;
    }, 2000)
}

loadBtn.addEventListener('click', delayedUpload);

const loadBtnMasonry = document.getElementById('load-more-btn-masonry');
const sectionSix = document.querySelector('.section-six');
const gridItems = document.querySelectorAll('.grid-item');
const gridContainer = document.querySelector('.grid');
const loaderMasonry = document.querySelector('.loader-masonry');

function loadMoreMasonry() {
    sectionSix.style.height = 2150 + 'px';  
    gridContainer.style.height = 2150 + 'px';
    loadBtnMasonry.style.display = 'none';
    gridItems.forEach(gridItem => {
        gridItem.classList.remove('masonry-inactive')
    })
}

let timeoutID;
function delayedUploadMasonry() {
    timeoutID = setTimeout(loadMoreMasonry, 2000);
    loaderMasonry.style.opacity = 100;
    setTimeout(() => {
        loaderMasonry.style.opacity = 0;
    }, 2000)
}

loadBtnMasonry.addEventListener('click', delayedUploadMasonry);