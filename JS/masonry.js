var elem = document.querySelector('.grid');
var msnry = new Masonry( elem, {
    fitWidth: true,
    gutter: 19,
    columnWidth: '.grid-sizer',
    itemSelector: '.grid-item'  
});