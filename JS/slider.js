const largePhotoSliderLine = document.querySelector('.large-photo-slider-line');
const reviewSlider = document.querySelector('.review-slider-line');
const nameSlider = document.querySelector('.name-slider-line');
const prevButton = document.querySelector('.prev');
const nextButton = document.querySelector('.next');
const photos = document.querySelectorAll('.photo');
const userBoxes = document.querySelectorAll('.user-box');
const reviews = document.querySelectorAll('.review');
const largePhotos = document.querySelectorAll('.large-photo');

let position = 0;
let photoIndex = 0;
let reviewPosition = 0;
let namePosition = 0;

const nextSlide = () => {
    if(position < 498 && reviewPosition < 180 && namePosition < 180){
        position += 166;
        photoIndex++;
        reviewPosition += 60;
        namePosition += 60
    } else {
        position = 0;
        photoIndex = 0;
        reviewPosition = 0;
        namePosition = 0;
    }    
    
    largePhotoSliderLine.style.left = -position + 'px';
    reviewSlider.style.top = -reviewPosition + 'px';
    nameSlider.style.top = -namePosition + 'px';
    thisPhoto(photoIndex);
    thisBox(photoIndex);
    thisReview(photoIndex);
    thisLargePhoto(photoIndex);
}

const prevSlide = () => {
    if(position > 0 && reviewPosition > 0 && namePosition > 0){
        position -= 166;
        photoIndex--;
        reviewPosition -= 60;
        namePosition -= 60
    } else {
        position = 498;
        photoIndex = 3;
        reviewPosition = 180;
        namePosition = 180
    }
    
    largePhotoSliderLine.style.left = -position + 'px';
    reviewSlider.style.top = -reviewPosition + 'px';
    nameSlider.style.top = -namePosition + 'px';
    thisPhoto(photoIndex);
    thisBox(photoIndex);
    thisReview(photoIndex);
    thisLargePhoto(photoIndex);
}

nextButton.addEventListener('click', nextSlide);
prevButton.addEventListener('click', prevSlide);

const thisPhoto = (index) => {
    for (let photo of photos) {
        photo.classList.remove('active-photo');
    }
    photos[index].classList.add('active-photo');
}

const thisBox = (index) => {
    for (let userBox of userBoxes) {
        userBox.classList.remove('lp-active');
    }
    userBoxes[index].classList.add('lp-active');
}

const thisReview = (index) => {
    for (let review of reviews) {
        review.classList.remove('lp-active');
    }
    reviews[index].classList.add('lp-active');
}

const thisLargePhoto = (index) => {
    for (let largePhoto of largePhotos) {
        largePhoto.classList.remove('lp-active');
    }
    largePhotos[index].classList.add('lp-active');
}

photos.forEach((photo, index) => {
    photo.addEventListener('click', () => {
        position = 166 * index;
        largePhotoSliderLine.style.left = -position + 'px';
        reviewPosition = 60 * index;
        reviewSlider.style.top = -reviewPosition + 'px';
        namePosition = 60 * index;
        nameSlider.style.top = -reviewPosition + 'px';
        photoIndex = index;
        thisPhoto(photoIndex);
        thisBox(photoIndex);
        thisReview(photoIndex);
        thisLargePhoto(photoIndex);
    })
})