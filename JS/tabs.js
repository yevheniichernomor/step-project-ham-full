const tabServiceMenuItem = document.querySelectorAll('.s-menu-item');
const tabServiceContent = document.querySelectorAll('.s-menu-text');
const tabServiceImage = document.querySelectorAll('.s-menu-image');
const tabTriangle = document.querySelectorAll('.t-box');

function tabs() {
    for(let i = 0; i < tabServiceMenuItem.length; i++){
        tabServiceMenuItem[i].addEventListener('click', (event) => {
            
            let tabCurrent = event.target.parentElement.children;
            for(let j = 0; j < tabCurrent.length; j++){
                tabCurrent[j].classList.remove('active-s-menu-item')
            }
            event.target.classList.add('active-s-menu-item');
    
            let contentCurrent = event.target.parentElement.parentElement.children[3].children;
            for(let c = 0; c < contentCurrent.length; c++){
                contentCurrent[c].classList.remove('text-active');
            }
    
            let imageCurrent = event.target.parentElement.parentElement.children[3].children[0].children;
            for(let d = 0; d < imageCurrent.length; d++){
                imageCurrent[d].classList.remove('text-active');
            }

            let triangleCurrent = event.target.parentElement.nextElementSibling.children;
            for (let t = 0; t < triangleCurrent.length; t++){
                triangleCurrent[t].classList.remove('t-box-active');
            }
    
            tabServiceImage[i].classList.add('text-active');
            tabServiceContent[i].classList.add('text-active');
            tabTriangle[i].classList.add('t-box-active');           
        });
    }
}

tabs();